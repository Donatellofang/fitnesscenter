﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;


public class PrepareGallery : MonoBehaviour
{


//     public List<Texture2D> allPicTex;
//     public List<Texture2D> allWordsTex;

    public Dictionary<string ,Texture2D> allPicTex;
    public Dictionary<string, Texture2D> allWordsTex;

    public List<string> nameOfButtons;
    public List<string> nameOfFloors;
    public List<int> testImagesWidget;
    public Dictionary<string, int> atlasNameToCount;

    private string floorName;
    private string btnName;
    

    void Awake()
    {
        s_Instance = this;

        testImagesWidget = new List<int>();
//         allPicTex = new List<Texture2D>();  //???? 2 is Window Pictures count
//         allWordsTex = new List<Texture2D>(); //????2 is Window Words count

        allPicTex = new Dictionary<string, Texture2D>();
        allWordsTex = new Dictionary<string, Texture2D>();
        atlasNameToCount = new Dictionary<string, int>();
    }
    // Use this for initialization
    void Start()
    {

        PreLoadResouces();
        OptionBehaviours.Instance.CloseOption();
    }



    void PreLoadResouces()
    {
        int i = 0;
        int last_i = 0;
        int j = 0;
        int last_j = 0;
        string indexSign;
        string atlasName;

        foreach (var floorItem in nameOfFloors)
        {
            foreach (var btnItem in nameOfButtons)
            {
                floorName = string.Format("{0}/", floorItem);
                btnName = string.Format("{0}", btnItem);
                
                //Debug.Log("floorName , btnName: " + floorName + " " + btnName);

                //Load Pictures Resources
                string imagePath = (UnityEngine.Application.streamingAssetsPath + "/Textures/" + floorName + btnName + "/IntroducePic/");
               
                string[] allImages = System.IO.Directory.GetFiles(imagePath,"*.png");
                
                i = 0;
                do 
                {
                    indexSign = string.Format("{0}/{1}/{2}", floorItem, btnItem,i);
                    atlasName = string.Format("{0}/{1}", floorItem, btnItem);

                    byte[] bytes = System.IO.File.ReadAllBytes(allImages[i]);
          
                    //allPicTex[i + last_i] = new Texture2D(10, 10, TextureFormat.RGB24, false);
                    allPicTex.Add(indexSign,new Texture2D(10, 10, TextureFormat.RGB24, false));
                    //allPicTex[i + last_i].LoadImage(bytes);
                    allPicTex[indexSign].LoadImage(bytes);
                    
                    i++;
                } while (i < allImages.Length);
                last_i = i;
                atlasNameToCount.Add(atlasName, last_i);


                //Load Words Resources
                string wordsPath = (UnityEngine.Application.streamingAssetsPath + "/Textures/" + floorName + btnName + "/IntroduceWords/");
                string[] allWords = System.IO.Directory.GetFiles(wordsPath, "*.png");
                j = 0;
                do 
                {
                    indexSign = string.Format("{0}/{1}/{2}", floorItem, btnItem, j);


                    byte[] wordsBytes = System.IO.File.ReadAllBytes(allWords[j]);
                    //allWordsTex[j + last_j] = new Texture2D(10, 10, TextureFormat.RGB24, false);
                    allWordsTex.Add(indexSign,new Texture2D(10, 10, TextureFormat.RGB24, false));
                    //allWordsTex[j + last_j].LoadImage(wordsBytes);

                    allWordsTex[indexSign].LoadImage(wordsBytes);

                    j++;
                } while (j < allWords.Length);
                last_j = j;


            }
        }
        foreach (KeyValuePair<string,int> kvp in atlasNameToCount)
        {
            testImagesWidget.Add(kvp.Value);
        }

    }



    // Update is called once per frame
    void Update()
    {

    }

    private static PrepareGallery s_Instance = null;
    public static PrepareGallery Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.Log("There is no PrepareGallery in scene");
                return null;
            }
        }
    }

}
