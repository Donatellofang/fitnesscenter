﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AssignUITexture : MonoBehaviour {

    public UITexture hintTex;
    public UITexture introductionTex;
    public UITexture mapTex;

    public List<Texture> hintTexs;
    public List<Texture> introductionTexs;
    public List<Texture> mapTexs;



	// Use this for initialization
	void Start () {

        AssignTex();
	}

    void AssignTex()
    {
        int floorNum = Panel3Behaviours.Instance.floorTags.IndexOf(transform.tag) + 1;
        MainManager.Instance.currentFloorTag = transform.tag;
        switch (floorNum)
        {
            case 1:
                hintTex.mainTexture = hintTexs[0];
                introductionTex.mainTexture=introductionTexs[0];
                mapTex.mainTexture=mapTexs[0];
                break;
            case 2:
                hintTex.mainTexture = hintTexs[1];
                introductionTex.mainTexture=introductionTexs[1];
                mapTex.mainTexture=mapTexs[1];
                break;
            case 3:
                hintTex.mainTexture = hintTexs[2];
                introductionTex.mainTexture=introductionTexs[2];
                mapTex.mainTexture=mapTexs[2];
                break;
            default:
                break;
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
