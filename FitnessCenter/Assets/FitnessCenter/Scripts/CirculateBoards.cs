﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;


public class CirculateBoards : MonoBehaviour {

    public bool startCirculate=false;
    public bool steady=true;
    public bool isRight=false;
    public bool isLeft = false;

    public List<string> boardsName;
    public List<GameObject> boards;

    public GameObject boardPrefab;
    public GameObject boardsParent;

    public GameObject m_LeftCircleBoard;
    public GameObject m_RightCircleBoard;

    public Vector3 m_LeftBoardPos;
    public Vector3 m_RightBoardPos;

    private float rightDifference;
    private float leftDifference;
    public float differenceValue;
    public Dictionary<bool,string> lastStateToName;
    public List<string> testTrueDir;

    void Awake()
    {
        s_Instance = this;
        m_LeftBoardPos = boards[0].transform.position;
        m_RightBoardPos = boards[boards.Count - 1].transform.position;
    }

	// Use this for initialization
	void Start () {
        rightDifference = Mathf.Abs(transform.position.x-PoleInfo.Instance.rightPole_x);
        leftDifference = Mathf.Abs(transform.position.x - PoleInfo.Instance.leftPole_x);
        lastStateToName=new Dictionary<bool,string>();
        testTrueDir = new List<string>();

	}
    public void Circulate(bool isRightDir)
    {
        Debug.Log("Enter Circulate");
        if (isRightDir)//Slide RightDirection
        {
            m_LeftCircleBoard = Instantiate(boardPrefab) as GameObject;
            m_LeftCircleBoard.transform.parent = boardsParent.transform;
            m_LeftCircleBoard.transform.position = m_LeftBoardPos;
            
            m_LeftCircleBoard.transform.localScale = Vector3.one;

            GameObject waitDestroyObject = boards[boards.Count - 1];

            boards.RemoveAt(boards.Count-1);
            boards.Insert(0,m_LeftCircleBoard);
            Destroy(waitDestroyObject);


        } 
        else//Slide LeftDirection
        {
            m_RightCircleBoard = Instantiate(boardPrefab) as GameObject;
            m_RightCircleBoard.transform.parent = boardsParent.transform;
            m_RightCircleBoard.transform.position = m_RightBoardPos;

            m_RightCircleBoard.transform.localScale = Vector3.one;

            GameObject waitDestroyObject = boards[0];

            boards.RemoveAt(0);
            boards.Insert(boards.Count,m_RightCircleBoard);
            Destroy(waitDestroyObject);
            

        }
        RenameBoardsItem();
        steady = true;
    }

    public void ExchangeBoard()
    {
        if (steady&&startCirculate)
        {
            //After auto Move to steady state           
            
            float compareValue =transform.position.x;
            Analysis();
            if (FloatEqualTo(compareValue+differenceValue,PoleInfo.Instance.rightPole_x,1f))//Right Direction
            {
                Debug.Log("To Right Pole");
                steady = false;
                Circulate(true);
                startCirculate = false;
                isRight = false;
                if (lastStateToName.Count != 0)
                {
                    testTrueDir.Add(lastStateToName[true]);
                }
                
            }
            else if (FloatEqualTo(compareValue + differenceValue, PoleInfo.Instance.leftPole_x, 1f))//Left Direction
            {
                Debug.Log("To Left Pole");
                steady = false;
                Circulate(false);
                startCirculate = false;
                isLeft = false;

                if (lastStateToName.Count != 0)
                {
                    testTrueDir.Add(lastStateToName[true]);
                }
            }
            

        }
        
        
    }
    public void RecordState()
    {
        lastStateToName.Clear();
        if (isLeft)
        {
            
            lastStateToName.Add(true, "isLeft");
        } 
        else if (isRight)
        {
            lastStateToName.Add(true, "isRight");

        }
        
        
    }
    void Analysis()
    {
        if (lastStateToName.Count==0)
        {
            differenceValue = 0;
        }
        else if (lastStateToName[true] == "isLeft")
        {
            differenceValue=leftDifference *1;
        }
        else
        {
            differenceValue = -1*rightDifference ;

        }
        
    }
    void RenameBoardsItem()
    {
        for(int i=0;i<boards.Count;i++)
        {
            boards[i].name = boardsName[i];
        }
    }
    public bool FloatEqualTo(float left, float right, float epsilon)
    {
        return Mathf.Abs(left*100 - right*100) <= epsilon;
    }

	// Update is called once per frame
	void Update () {
        
        ExchangeBoard();
	}

    private static CirculateBoards s_Instance = null;
    public static CirculateBoards Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.Log("There is no CirculateBoards in scene");
                return null;
            }
        }
    }
}
