﻿using UnityEngine;
using System.Collections;

public class UpdateUITexture : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
        UITexture ui=this.gameObject.GetComponent<UITexture>();
        if (ui!=null)
        {
            string url = @"file://" + (UnityEngine.Application.streamingAssetsPath + "/Textures/" + ui.mainTexture.name + ".png").Replace("/","\\");
            Debug.Log("Load:" + url);
            StartCoroutine(LoadUI(url, ui));
        }
        
	}
    IEnumerator LoadUI(string url,UITexture tex)
    {
        WWW www = new WWW(url);
        yield return www;
        if (www.isDone && www.error==null)
        {
            tex.mainTexture = www.texture as Texture;
        } 
        else
        {
            Debug.Log("Load Error!");
        }

    }
	// Update is called once per frame
	void Update () {
	
	}

}
