﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;

public class OptionBehaviours : MonoBehaviour
{

    public string currentOptionTag;

    public List<GameObject> floorMapOptions;

    public List<GameObject> otherOptionObjects;


    private List<string> m_OtherOptionTag;


    void Awake()
    {
        s_Instance = this;
    }
    // Use this for initialization
    void Start()
    {

        m_OtherOptionTag = new List<string>();
        foreach (var item in otherOptionObjects)
        {
            m_OtherOptionTag.Add(item.tag);
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator AnalysisCurrentOption(int panelShowOrdinal) //Which Option of Return or Next
    {
        yield return new WaitForSeconds(0.7f);
        yield return new WaitForEndOfFrame();

        if (currentOptionTag == m_OtherOptionTag[0])  //To Previous Panel
        {
            PanelManager.Instance.ShowPanel(panelShowOrdinal - 2);
        }
        else if (currentOptionTag == m_OtherOptionTag[1])//Close Button
        {
            CloseOption();
            PanelManager.Instance.ShowPanel(panelShowOrdinal);//panelShowOrdinal equal to 3
        }
//         else if (currentOptionTag == m_OtherOptionTag[2])//Close Button
//         {
//             
//         }
//         else if (currentOptionTag == m_OtherOptionTag[3])//Close Button
//         {
//             
//         }
        else //To Next Panel
        {

            if (panelShowOrdinal == 4)//Current Show Panel is Panel4
            {

                //Debug.Log("Left and Right Button///////");
                PanelManager.Instance.allPanels[PanelManager.Instance.allPanels.Count - 1].SetActiveRecursively(true);

                //After GET currentFloorTag,currentBtnName;
                if (PrepareGallery.Instance != null)
                {
                    ShowGallery.Instance.Show();
                }

                PanelManager.Instance.CtrlEnterPanel4();
            }
            else
            {
                PanelManager.Instance.ShowPanel(panelShowOrdinal);//3 is Panel3
                if (panelShowOrdinal == 3) //Current Panel is Panel2
                {
                    PanelManager.Instance.CtrlEnterPanel3(currentOptionTag);
                }
            }

        }



    }

    public void CloseOption() //recover Previous frozen Panel
    {
        PanelManager.Instance.allPanels[PanelManager.Instance.allPanels.Count - 1].SetActiveRecursively(false);
        BoxCollider[] colliderArray = PanelManager.Instance.allPanels[2].GetComponentsInChildren<BoxCollider>();
        foreach (var item in colliderArray)
        {
            item.enabled = true;
        }
    }



    private static OptionBehaviours s_Instance = null;
    public static OptionBehaviours Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.LogError("There is no OptionBehaviours Instance in the scene");

                return null;
            }
        }

    }


}
