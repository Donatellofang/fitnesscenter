﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;

public class PanelManager : MonoBehaviour
{

    public List<GameObject> allPanels;

    public delegate void ButtonBehavioursDelegate();



    private int m_CurrentShowPanel;


    void Awake()
    {
        s_Instance = this;
    }
    // Use this for initialization
    void Start()
    {

        ShowPanel(1);

    }

    // Update is called once per frame
    void Update()
    {

    }



    public void ManagePanel()
    {
        GetExistPanel();
        switch (m_CurrentShowPanel)
        {

            case 0://1,Show Panel2
                StartCoroutine(OptionBehaviours.Instance.AnalysisCurrentOption(m_CurrentShowPanel + 2));//currentShowPanel+1 is real current panelNum 
                break;
            case 1://2,Show Panel3   
                StartCoroutine(OptionBehaviours.Instance.AnalysisCurrentOption(m_CurrentShowPanel + 2));

                break;
            case 2://3,Show Panel4   
                StartCoroutine(OptionBehaviours.Instance.AnalysisCurrentOption(m_CurrentShowPanel + 2));
                break;
            case 3://4,Show Panel1   
                StartCoroutine(OptionBehaviours.Instance.AnalysisCurrentOption(m_CurrentShowPanel));//Switch to first panel
                break;
            default:
                break;
        }

    }




   public void CtrlEnterPanel3(string optionTag)
    {

        Panel3Behaviours.Instance.SwitchFloor(Panel2Behaviours.Instance.floorNameToSwitchNum[optionTag]);

    }

    public void CtrlEnterPanel4()
    {
        BoxCollider[] colliderArray = allPanels[2].GetComponentsInChildren<BoxCollider>();
        foreach (var item in colliderArray)
        {
            item.enabled = false;
        }

    }




    public void ShowPanel(int switchNum)
    {
        switchNum -= 1;//for list Index ordinal
        ResetAllExistPanel();
        allPanels[switchNum].SetActive(true);//Real Panel Ordinal subtract allPanel Ordinal equal 1;
    }

    void GetExistPanel()
    {
        foreach (var item in allPanels)
        {
            if (item.activeInHierarchy)
            {
                m_CurrentShowPanel = allPanels.IndexOf(item);
            }


        }

    }

    void ResetAllExistPanel()
    {
        foreach (var item in allPanels)
        {
            item.SetActive(false);
        }
    }



    private static PanelManager s_Instance = null;
    public static PanelManager Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.LogError("There is no PanelManager Instance in the scene");

                return null;
            }
        }

    }


}
