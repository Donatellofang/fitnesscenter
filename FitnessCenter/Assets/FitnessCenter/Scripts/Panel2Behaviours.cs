﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;


public class Panel2Behaviours : MonoBehaviour {

    public List<GameObject> floors;
    public Dictionary<string, int> floorNameToSwitchNum;



    void Awake()
    {
        s_Instance = this;
    }
	// Use this for initialization
	void Start () {


        floorNameToSwitchNum = new Dictionary<string, int>();
        int i=0;
	  foreach (var item in floors)
	  {
          floorNameToSwitchNum.Add(item.tag, i);
          i++;
	  }


        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    private static Panel2Behaviours s_Instance = null;
    public static Panel2Behaviours Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.LogError("There is no Panel2Behaviours in scene");
                return null;
            }
        }
    }
}
