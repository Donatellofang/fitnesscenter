﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;


public class ShowGallery : MonoBehaviour {

    public List<UITexture> windowPic;
    public List<UITexture> windowWords;

    public List<Texture2D> currentAtlas;

    private string m_currentBtnName;
    private string m_currentFloorTag;
    private string scourceIndex;
    private TweenBasic windowTweener;

    void Awake()
    {
        s_Instance = this;
    }
	// Use this for initialization
	void Start () {
        currentAtlas = new List<Texture2D>();
	}
    public void Show()
    {
        //FetchBtnSign();
        Prepare();
        ShowingEffect();

    }
    void ShowingEffect()
    {
        //Recover TweenScale 
        //transform.GetComponent<TweenScale>().enabled = true;
        transform.GetComponent<TweenScale>().ResetToBeginning();
        transform.GetComponent<TweenScale>().Play(true);
    }

    
    void Prepare()
    {
        FetchBtnSign();
        PrepareAtlas();
        PreparePic();
        //PrepareWords();

    }
    void FetchBtnSign()
    {
        m_currentBtnName = MainManager.Instance.currentBtnName;
        m_currentFloorTag = MainManager.Instance.currentFloorTag;

        scourceIndex = string.Format("{0}/{1}/0", m_currentFloorTag, m_currentBtnName);
    }

    void PrepareAtlas()
    {
        string index = string.Format("{0}/{1}", m_currentFloorTag, m_currentBtnName);
        int picsCount = PrepareGallery.Instance.atlasNameToCount[index];
        for (int i = 0; i < picsCount; i++)
        {
            string nameIndex = string.Format("{0}/{1}/{2}", m_currentFloorTag, m_currentBtnName, i);
            currentAtlas.Add(PrepareGallery.Instance.allPicTex[nameIndex]);
        }


    }

    void PreparePic()
    {
        int i = 0;
        foreach (var item in windowPic)
        {
            /*item.mainTexture = PrepareGallery.Instance.allPicTex[scourceIndex];*/
            item.mainTexture = currentAtlas[i];
            i++;
        }

       

    }
    void PrepareWords()
    {
        foreach (var item in windowWords)
        {
            item.mainTexture = PrepareGallery.Instance.allWordsTex[scourceIndex];
        }

    }
	// Update is called once per frame
	void Update () {
	
	}
    
    private static ShowGallery s_Instance = null;
    public static ShowGallery Instance
    {
        get
        {
            if (s_Instance != null)
            {
                return s_Instance;
            }
            else
            {
                GUILogDisplay.Log("There is no ShowGallery in scene");
                return null;
            }
        }
    }

}
