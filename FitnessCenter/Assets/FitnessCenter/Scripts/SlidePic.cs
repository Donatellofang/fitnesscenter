﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using TouchScript.Gestures;
using Wshrzzz.UnityUtil;

public class SlidePic : MonoBehaviour {


    public float xVelocity=20f;
    public float smoothTime = 10f;

    public List<UITexture> boardTexSequence;
    public int[] showSequence;
    public List<int> numSequence;
   
    private float limit_Y;
    private float object_x;
    public bool autoMove=false;
    private TweenBasic moveTweener;
    private bool startMove=false;


	// Use this for initialization
	void Start () {

        numSequence = new List<int>();
        showSequence=new int[3];
        //InitializeSequence();
        limit_Y = transform.localPosition.y;

   
	}
    private void OnEnable()//at their Parent Script add the Handlers
    {
        GetComponent<PressGesture>().Pressed += pressedHandler;
        GetComponent<ReleaseGesture>().Released += releasedHandler;
    }

    private void OnDisable()//at their Parent Script subtract the Handlers
    {

        GetComponent<PressGesture>().Pressed -= pressedHandler;
        GetComponent<ReleaseGesture>().Released -= releasedHandler;
    }

    private void pressedHandler(object sender, EventArgs e)
    {
        autoMove = false;
        CirculateBoards.Instance.startCirculate = true;
    }

    private void releasedHandler(object sender, EventArgs e)
    {
        autoMove = true;
        startMove = true;
    }
    IEnumerator AnalysisCurrentPos()
    {
        object_x = transform.position.x;

        if (object_x*100<PoleInfo.Instance.centerLeftPole_x*100)//Left Direction
        {
            Debug.Log("//Left Direction");
            //transform.position = new Vector3(Mathf.SmoothDamp(transform.position.x, PoleInfo.Instance.leftPole_x, ref xVelocity, smoothTime),transform.position.y,0);
            yield return StartCoroutine(Move(PoleInfo.Instance.leftPole_x));
            CirculateBoards.Instance.isLeft= true;


        }
        else if (object_x * 100 > PoleInfo.Instance.centerRightPole_x * 100)//Right Direction
        {
            Debug.Log("//Right Direction");
            //transform.position = new Vector3(Mathf.SmoothDamp(transform.position.x, PoleInfo.Instance.rightPole_x, ref xVelocity, smoothTime), transform.position.y, 0);
            yield return StartCoroutine(Move(PoleInfo.Instance.rightPole_x));
            CirculateBoards.Instance.isRight = true;


            
        }
        else if (!Mathf.Approximately(object_x * 100, PoleInfo.Instance.centerPole_x * 100))
        {
            //transform.position = new Vector3(Mathf.SmoothDamp(transform.position.x, PoleInfo.Instance.centerPole_x, ref xVelocity, smoothTime), transform.position.y, 0);
            yield return StartCoroutine(Move(PoleInfo.Instance.centerPole_x));
            
        }
        CirculateBoards.Instance.RecordState();
             
        
    }

    IEnumerator Move(float pole_x)
    {

        if (moveTweener == null)
        {
            moveTweener = TweenBasic.InitTweener(gameObject);
        }

        moveTweener.StartTween(TweenBasic.TweenType.Linear, smoothTime, reset: false);
        do
        {
            transform.position = new Vector3(moveTweener.SimpleLerp(transform.position.x, pole_x), transform.position.y, 0);
            //photoTex.transform.localScale = moveTweener.SimpleLerp(Vector3.one * 1.2f, Vector3.one);

            yield return new WaitForEndOfFrame();
        } while (moveTweener.IsTweening);
    }

    /*void ExchangeBoard(bool isleft)
    {
        //boardTexSequence.
         //Turn Left
        DealNumSequence(isleft);
        for (int i = 0; i < showSequence.Length;i++ )
        {
            showSequence[i]=showSequence[i]%
        }
         

        //Turn Right

    }
    void InitializeSequence()
    {
        int count = ShowGallery.Instance.currentAtlas.Count;
        for (int i = 0; i < count; i++)
        {
            numSequence.Add(i);

        }
        for (int i = 0; i < showSequence.Length;i++ )
        {
            showSequence[i] = i;
        }
    }
    void DealNumSequence(bool isLeft)
    {
        int count = ShowGallery.Instance.currentAtlas.Count;
        if (isLeft)
        {
            for (int i = 0; i < count; i++)
            {
                numSequence.Insert(i, i);
                //numSequence.RemoveAt(count);
            }
        } 
        else
        {
            for (int i = 0; i < count; i++)
            {
                numSequence.Add(i);
                //numSequence.RemoveAt(0);
            }
        }
        
        
    }*/
	// Update is called once per frame
	void Update () {
        

        transform.localPosition = new Vector3(transform.localPosition.x,Mathf.Lerp(transform.localPosition.y,limit_Y,1),0);
        if (autoMove==true&&startMove)
        {
            startMove = false;
            StartCoroutine(AnalysisCurrentPos());
            
        }
        
	}
}
