﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;
using TouchScript.Gestures;

public class ButtonBehaviours : MonoBehaviour
{


    private TweenBasic photoTweener;
    private float m_passedTime;
    private float m_EffectTime=1f;
    private bool m_Continue=false;
    // Use this for initialization
    void Start()
    {

        GetComponent<TapGesture>().Tapped += HandleTapped;

    }
    // Update is called once per frame
    void Update()
    {

    }


    void HandleTapped(object sender, System.EventArgs e)
    {
        
        StartCoroutine(ScaleHover());

        MainManager.Instance.currentBtnName = transform.name;
        OptionBehaviours.Instance.currentOptionTag = transform.tag;
        PanelManager.Instance.ManagePanel();

       

    }


    public IEnumerator ScaleHover()
    {
        if (photoTweener == null)
        {
            photoTweener = TweenBasic.InitTweener(gameObject);
        }

        photoTweener.StartTween(TweenBasic.TweenType.EasyInOut, 0.2f, reset: false);
        do
        {
            transform.localScale = photoTweener.SimpleLerp(Vector3.one, Vector3.one * 1.2f);

            yield return new WaitForEndOfFrame();
        } while (photoTweener.IsTweening);

        yield return new WaitForSeconds(0.3f);
        yield return StartCoroutine(ScaleNormal());

    }



    IEnumerator ScaleNormal()
    {
        if (photoTweener == null)
        {
            photoTweener = TweenBasic.InitTweener(gameObject);
        }

        photoTweener.StartTween(TweenBasic.TweenType.EasyInOut, 0.2f, reset: false);
        do
        {
            transform.localScale = photoTweener.SimpleLerp(Vector3.one * 1.2f, Vector3.one);

            yield return new WaitForEndOfFrame();
        } while (photoTweener.IsTweening);
    }

    
}
