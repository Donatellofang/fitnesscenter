﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Wshrzzz.UnityUtil;


public class Panel3Behaviours : MonoBehaviour {

    public GameObject floorPrefab;

    public List<string> floorTags;

    private GameObject m_Floor;

    void Awake()
    {
        s_Instance = this;
    }
	// Use this for initialization
	void Start () {
        
	  
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    
    public void SwitchFloor(int switchNum)
    {
        switch (switchNum)
        {
            case 0://0--1F
                ShowFloor(1);
                break;
            case 1://1--2F
                ShowFloor(2);
                break;
            case 2://2--3F
                ShowFloor(3);
                break;
            default:
                break;
        }

    }
    void ShowFloor(int floorNum)
    {
        
        ResetFloorsShowing();
        CreateFloor(floorNum);
        
       

    }

    void CreateFloor(int floorNum)
    {
        m_Floor = Instantiate(floorPrefab) as GameObject;
        m_Floor.transform.parent = transform;
        m_Floor.transform.localScale= Vector3.one;
        switch (floorNum)
        {
            case 1:
                m_Floor.tag = floorTags[0];//1F
                m_Floor.name = floorTags[0]; //Tags Name
                break;
            case 2:
                m_Floor.tag = floorTags[1];//2F
                m_Floor.name = floorTags[1];
                break;
            case 3:
                m_Floor.tag = floorTags[2];//3F
                m_Floor.name = floorTags[2];
                break;
            default:
                break;
        }


    }


    void ResetFloorsShowing()
    {
        Destroy(m_Floor);
    }

    void ShowFloorPic()
    {

    }

    private static Panel3Behaviours s_Instance=null;
    public static Panel3Behaviours Instance
    {
        get
        {
            if (s_Instance!=null)
            {
                return s_Instance;
            } 
            else
            {
                GUILogDisplay.Log("There is no Panel3Behaviours in scene");
                return null;
            }
        }
    }
}
